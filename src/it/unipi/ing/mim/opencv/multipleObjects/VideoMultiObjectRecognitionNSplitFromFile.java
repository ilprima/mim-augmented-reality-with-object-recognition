package it.unipi.ing.mim.opencv.multipleObjects;

import it.unipi.ing.mim.opencv.BoundingBox;
import it.unipi.ing.mim.opencv.Parameters;
import it.unipi.ing.mim.opencv.localFeatures.*;
import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacv.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

public class VideoMultiObjectRecognitionNSplitFromFile {


    private KeyPointsDetector detector = new KeyPointsDetector();
    private FeaturesExtraction extractor = new FeaturesExtraction();
    private FeaturesMatching matching = new FeaturesMatching();
    private FeaturesMatchingFiltered filter = new FeaturesMatchingFiltered();
    private Ransac ransac = new Ransac();

    private FFmpegFrameGrabber frameGrabber;

    List<Mat> trainingObjs;
    List<Mat> descriptorsTrainingObjects;
    List<Integer> fastIndex; // è la lista degli indici trovati nell'ultima scansione
    List<KeyPointVector> keypointsTrainingObjects;

    //lista che mantiene, per ogni file del dataset, il nome associato, Inoltre crea random un colore per ogni oggetto
    List<String> trainingObjPath;
    List<Scalar> trainingObjColors;


    public CanvasFrame canvas;

    public static void main(String[] args) throws Exception {

        VideoMultiObjectRecognitionNSplitFromFile objectRecognition = new VideoMultiObjectRecognitionNSplitFromFile();

        objectRecognition.start();
    }


    public VideoMultiObjectRecognitionNSplitFromFile() throws Exception {

        init();

        System.out.println("Opening camera");
        canvas = new CanvasFrame(Parameters.appTitle);
        canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

        frameGrabber = FFmpegFrameGrabber.createDefault(new File(Parameters.videoFromFilePath));
        frameGrabber.start();
    }

    public void start() throws FrameGrabber.Exception {
        OpenCVFrameConverter.ToMat toMat = new OpenCVFrameConverter.ToMat();

        int frameCounter = 0;
        double actual = 0;
        Frame frame;
        while ((frame = frameGrabber.grabFrame()) != null) {
            if (Parameters.FRAME_FREQ == 0 || frameCounter++ % Parameters.FRAME_FREQ == 0) {
                Mat matFrame = toMat.convert(frame);
                double start = actual / Parameters.splitFactor;
                double end = (actual + 1) / Parameters.splitFactor;
                actual = (actual + 1) % Parameters.splitFactor;
                System.out.println("actual è:" + actual);
                System.out.println("start è:" + start);
                System.out.println("end è:" + end);

                fastIndexScan(matFrame);// fa uno scan sulla lista composta da pochi oggetti
                homographyMultipleObjects(matFrame, start, end);// fa lo scan su una parte del datasety

                canvas.showImage(toMat.convert(matFrame));
            }
        }
        canvas.dispose();
    }


    //si crea la lista di oggetti, la lista dei descriptiors e dei keypoints
    private void init() {
        trainingObjPath = new ArrayList<>();
        trainingObjColors = new ArrayList<>();
        trainingObjs = this.getAllObjects();
        fastIndex = new ArrayList<Integer>();
        descriptorsTrainingObjects = extractor.extractDescriptorsFromObjs(Parameters.SRC_FOLDER);
        keypointsTrainingObjects = detector.detectKeypointsFromObjs(Parameters.SRC_FOLDER);
    }

    //crea la lista di oggetti del dataset
    List<Mat> getAllObjects() {

        System.out.println("Creo lista oggetti, lista nome e colori");

        List<Mat> descs = new ArrayList<Mat>();

        File[] folders = Parameters.SRC_FOLDER.listFiles();

        for (File imgFolder : folders) {
            File[] imgFiles = imgFolder.listFiles();
            if (imgFiles != null) {
                Scalar color = getARandomColor();
                for (File imgFile : imgFiles) {
                    try {
                        long time = -System.currentTimeMillis();
                        Mat frame = imread(imgFile.getPath());
                        time += System.currentTimeMillis();
                        System.out.println(time);
                        descs.add(frame);

                        //mantengo il nome del file associato al Mat
                        trainingObjPath.add(imgFile.getName().split("_")[0]);
                        //     trainingObjPath.add(imgFile.getName());
                        //un colore per ogni classe/directory
                        trainingObjColors.add(color);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return descs;

    }

    // fa lo scan su una lista di oggetti riconosciuta precedentemente dalla funzione homography
    public void fastIndexScan(Mat frame) {
        // detect keypoints from frame image
        KeyPointVector keypointsQuery = detector.detectKeypoints(frame);
        // extract features from keypoints
        Mat descQuery = extractor.extractDescriptor(frame, keypointsQuery);

        String lastName = "";
        for (int i = 0; i < fastIndex.size(); i++) {


            DMatchVector matches = matching.match(descriptorsTrainingObjects.get(fastIndex.get(i)), descQuery);

            DMatchVector goodMatching = filter.matchWithFiltering(matches, Parameters.MAX_DISTANCE_THRESHOLD);

            // use computeHomography the check the Homography

            if (goodMatching.size() > Parameters.MIN_GOOD_MATCHES) {
                ransac = new Ransac();
                System.out.println("Oggetto: " + this.trainingObjPath.get(fastIndex.get(i)));
                System.out.println("goodMatching > min: " + goodMatching.size());

                ransac.computeHomography(goodMatching, this.keypointsTrainingObjects.get(fastIndex.get(i)), keypointsQuery);

                // and return it if there are enough
                Mat homography = null;
                homography = ransac.getHomography();

                System.out.println("conto num Inliers: " + ransac.countNumInliers());

                if (ransac.countNumInliers() >= Parameters.MIN_RANSAC_INLIERS) {

                    boolean canBB = false;
                    if (lastName.equals("")) //first time
                        canBB = true;
                    else { //is not first time
                        if (lastName.equals(trainingObjPath.get(fastIndex.get(i)))) //is the same class
                            canBB = false;
                        else
                            canBB = true;
                    }

                    if (canBB) {
                        BoundingBox.addBoundingBox(frame, this.trainingObjs.get(fastIndex.get(i)), homography, trainingObjPath.get(fastIndex.get(i)), trainingObjColors.get(fastIndex.get(i)), Parameters.setOverlay);
                        lastName = trainingObjPath.get(fastIndex.get(i));
                    }

                }

            }
        }


    }

    //homography per riconoscimento più di 1 oggetto nel frame video
    public void homographyMultipleObjects(Mat frame, double start, double end) {


        // detect keypoints from frame image
        KeyPointVector keypointsQuery = detector.detectKeypoints(frame);

        // extract features from keypoints
        Mat descQuery = extractor.extractDescriptor(frame, keypointsQuery);
        String lastName = "";

        start = start * this.descriptorsTrainingObjects.size();
        end = end * this.descriptorsTrainingObjects.size();

        for (int i = (int) start; i < (int) end; i++) {

            DMatchVector matches = matching.match(descriptorsTrainingObjects.get(i), descQuery);
            DMatchVector goodMatching = filter.matchWithFiltering(matches, Parameters.MAX_DISTANCE_THRESHOLD);

            // use computeHomography the check the Homography
            if (goodMatching.size() > Parameters.MIN_GOOD_MATCHES) {
                ransac = new Ransac();
                System.out.println("Oggetto: " + this.trainingObjPath.get(i));
                System.out.println("goodMatching > min: " + goodMatching.size());

                ransac.computeHomography(goodMatching, this.keypointsTrainingObjects.get(i), keypointsQuery);

                // and return it if there are enough
                Mat homography = null;
                homography = ransac.getHomography();

                System.out.println("conto num Inliers: " + ransac.countNumInliers());

                if (ransac.countNumInliers() >= Parameters.MIN_RANSAC_INLIERS) {

                    //utilizza lastName che contine il nome della classe se è già stata trovata non disegna altre BB
                    boolean canBB = false;
                    if (lastName.equals("")) //first time
                        canBB = true;
                    else { //is not first time
                        if (lastName.equals(trainingObjPath.get(i))) //is the same class
                            canBB = false;
                        else
                            canBB = true;
                    }
                    if (canBB) {
                        BoundingBox.addBoundingBox(frame, this.trainingObjs.get(i), homography, trainingObjPath.get(i), trainingObjColors.get(i), Parameters.setOverlay);
                        lastName = trainingObjPath.get(i);
                        if (fastIndex.size() == Parameters.maxDimFastIndex) {
                            fastIndex.remove(0);
                        }
                        fastIndex.add(i);
                    }
                }
            }
        }
    }


    Scalar getARandomColor() {
        Random random = new Random();

        int blue = random.nextInt(255);
        int green = random.nextInt(255);
        int red = random.nextInt(255);
        return new Scalar(red, green, blue, 1);

    }
}
