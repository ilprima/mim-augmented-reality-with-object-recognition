package it.unipi.ing.mim.opencv.localFeatures;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_features2d.Feature2D;
import org.bytedeco.javacpp.opencv_features2d.ORB;
import org.bytedeco.javacpp.indexer.UByteRawIndexer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FeaturesExtraction {

    private Feature2D descExtractor;


    //TODO
    public FeaturesExtraction() {
        //initialize descExtractor;

        this.descExtractor = ORB.create();
        ((ORB) descExtractor).setMaxFeatures(1000);
    }


    public Mat extractDescriptor(Mat img, KeyPointVector keypoints) {
        //extract the visual features

        Mat descriptor = new Mat();
        descExtractor.compute(img, keypoints, descriptor);

        return descriptor;
    }


    //Estrae i descrittori per ogni immagine del dataset contenuto nella cartella data/dataset

    public List<Mat> extractDescriptorsFromObjs(File srcFolder) {
        System.out.println("Estraggo descrittori");

        List<Mat> descs = new ArrayList<Mat>();

        File[] folders = srcFolder.listFiles();

        int counter = 1;
        for (File imgFolder : folders) {
            System.out.println(imgFolder.getName());
            File[] imgFiles = imgFolder.listFiles();
            if (imgFiles != null) {

                for (File imgFile : imgFiles) {
                    System.out.println(counter++ + " - extracting " + imgFile.getName());
                    try {
                        long time = -System.currentTimeMillis();

                        Mat frame = imread(imgFile.getPath());


                        KeyPointsDetector detector = new KeyPointsDetector();

                        KeyPointVector keypoints = detector.detectKeypoints(frame);

                        // extract features from keypoints

                        FeaturesExtraction extractor = new FeaturesExtraction();

                        Mat descQuery = extractor.extractDescriptor(frame, keypoints);

                        time += System.currentTimeMillis();
                        System.out.println(time);

                        descs.add(descQuery);
                        System.out.println("Num di oggetti: " + descs.size());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        return descs;
    }

}