package it.unipi.ing.mim.opencv.localFeatures;

import static org.bytedeco.javacpp.opencv_features2d.drawKeypoints;
import static org.bytedeco.javacpp.opencv_highgui.destroyAllWindows;
import static org.bytedeco.javacpp.opencv_highgui.imshow;
import static org.bytedeco.javacpp.opencv_highgui.waitKey;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import it.unipi.ing.mim.opencv.BoundingBox;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags;
import org.bytedeco.javacpp.opencv_features2d.Feature2D;
import org.bytedeco.javacpp.opencv_features2d.ORB;

import java.io.File;
import java.util.ArrayList;
import java.util.List;




/*
*
*
* Fa la detection dei keypoints e li disegna, per la detection utilizza ORB
*
*
* */


public class KeyPointsDetector {

	private Feature2D detector;

	
	//TODO
	public KeyPointsDetector() {
		//initialize detector

		this.detector = ORB.create();
		((ORB)detector).setMaxFeatures(1000);

	}

	//TODO
	public KeyPointVector detectKeypoints(Mat img) {
		//Detect img keypoints

		KeyPointVector keypoints = new KeyPointVector();
		detector.detect(img, keypoints);

		return keypoints;
	}

	//TODO
	public void printKeyPointsValues(KeyPointVector keypoints) {
		//Print keypoint data for each keypoint: x, y, size and angle

		for (int i=0; i<keypoints.size(); i++){
			System.out.println("Size: " + keypoints.get(i).size());
			System.out.println("Angle: " + keypoints.get(i).angle());
			System.out.println("x: " + keypoints.get(i).pt().x());
			System.out.println("y: " + keypoints.get(i).pt().x());
			System.out.println("");
		}
	}




	// fa la detection dei keypoints di ogni oggetto nella cartella data/dataset
	public List<KeyPointVector> detectKeypointsFromObjs (File srcFolder){
		System.out.println("Detection dei keypoints");
		List<KeyPointVector>  keypoints = new ArrayList<KeyPointVector>();

		File[] folders = srcFolder.listFiles();

		int counter = 1;
		for (File imgFolder: folders) {
			System.out.println(imgFolder.getName());
			File[] imgFiles = imgFolder.listFiles();
			if (imgFiles != null){

				for (File imgFile : imgFiles) {


					try {

						Mat frame = imread(imgFile.getPath());

						KeyPointsDetector detector = new KeyPointsDetector();

						KeyPointVector kp = detector.detectKeypoints(frame);

						keypoints.add(kp);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}


		return keypoints;
	}

}
