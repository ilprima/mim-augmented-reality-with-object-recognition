package it.unipi.ing.mim.opencv.localFeatures;

import it.unipi.ing.mim.opencv.BoundingBox;
import it.unipi.ing.mim.opencv.Parameters;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.DMatchVector;

import java.util.List;
import java.util.concurrent.Callable;

public class Homography implements Runnable {

    private Mat trainingObj;
    private Mat descriptorsTrainingObject; // training object associato al thread
    private Mat descQuery; // description query
    private String trainingObjPath;
    private Scalar trainingObjColor;
    private KeyPointVector keypointsTrainingObject;
    private KeyPointVector keypointsQuery;
    private FeaturesMatching matching = new FeaturesMatching();
    private FeaturesMatchingFiltered filter = new FeaturesMatchingFiltered();
    private Ransac ransac = new Ransac();
    private Mat frame;


    public Homography(Mat trainingObj, Mat descriptorsTrainingObject, Mat descQuery, String trainingObjPath, Scalar trainingObjColor, KeyPointVector keypointsTrainingObject, KeyPointVector keypointsQuery, Mat frame) {
        this.trainingObj = trainingObj;
        this.descriptorsTrainingObject = descriptorsTrainingObject;
        this.descQuery = descQuery;
        this.trainingObjPath = trainingObjPath;
        this.trainingObjColor = trainingObjColor;
        this.keypointsTrainingObject = keypointsTrainingObject;
        this.keypointsQuery = keypointsQuery;
        this.frame = frame;
    }


    @Override
    public void run() {

        DMatchVector matches = matching.match(descriptorsTrainingObject, descQuery);
        DMatchVector goodMatching = filter.matchWithFiltering(matches, Parameters.MAX_DISTANCE_THRESHOLD);

        // use computeHomography the check the Homography

        if (goodMatching.size() > Parameters.MIN_GOOD_MATCHES) {
            ransac = new Ransac();
            System.out.println("Oggetto: " + trainingObjPath);
            System.out.println("goodMatching > min: " + goodMatching.size());

            ransac.computeHomography(goodMatching, keypointsTrainingObject, keypointsQuery);

            // and return it if there are enough
            Mat homography = null;
            homography = ransac.getHomography();

            System.out.println("conto num Inliers: " + ransac.countNumInliers());

            if (ransac.countNumInliers() >= Parameters.MIN_RANSAC_INLIERS) {

                /* //utilizza lastName che contine il nome della classe se è già stata trovata non disegna altre BB
                boolean canBB = false;
                if (lastName.equals("")) //first time
                    canBB = true;
                else { //is not first time
                    if (lastName.equals(trainingObjPath.get(i))) //is the same class
                        canBB = false;
                    else
                        canBB = true;
                }
                */
                //              if (canBB) {
                BoundingBox.addBoundingBox(frame, trainingObj, homography, trainingObjPath, trainingObjColor,Parameters.setOverlay);
                //              lastName = trainingObjPath.get(i);
                //            }

            }
        }

    }

}
