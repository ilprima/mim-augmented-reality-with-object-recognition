package it.unipi.ing.mim.opencv.localFeatures;

import static org.bytedeco.javacpp.opencv_features2d.drawMatches;
import static org.bytedeco.javacpp.opencv_highgui.destroyAllWindows;
import static org.bytedeco.javacpp.opencv_highgui.imshow;
import static org.bytedeco.javacpp.opencv_highgui.waitKey;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import it.unipi.ing.mim.opencv.BoundingBox;
import org.bytedeco.javacpp.opencv_core.DMatch;
import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;

import it.unipi.ing.mim.opencv.Parameters;


/*
*  Esegue il matching tra due immagini filtrando quelle con distanza sopra la threshold
*
*  It takes a threshold and matches as input and returns a DMatchVector with the best matches (under the threshold)
*  In our example setup, the number of matched descriptor filtered should be 145
*  At the beginning setup the size of goodMatches = to the size of the input matches then use the method DMatchVector.resize to shrink
*  it to the size of the good matches found
*
* */

public class FeaturesMatchingFiltered {



	public DMatchVector matchWithFiltering(DMatchVector matches, int threshold) {
		//return the good matches

		long goodMatchesSize = matches.size();

		DMatchVector goodMatches = new DMatchVector(goodMatchesSize);

		int count = 0;

		for (long i =0; i<matches.size(); i++){
			DMatch curr = matches.get(i);
			if (curr.distance()<threshold){
				goodMatches.put(count, curr);
				count++;
			}

		}

		goodMatches.resize(count);


		return goodMatches;
	}

}