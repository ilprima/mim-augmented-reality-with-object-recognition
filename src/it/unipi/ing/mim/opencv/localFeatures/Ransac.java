package it.unipi.ing.mim.opencv.localFeatures;

import static org.bytedeco.javacpp.opencv_calib3d.RANSAC;
import static org.bytedeco.javacpp.opencv_calib3d.findHomography;
import static org.bytedeco.javacpp.opencv_core.CV_32FC2;
import static org.bytedeco.javacpp.opencv_core.CV_8UC1;
import static org.bytedeco.javacpp.opencv_features2d.drawMatches;
import static org.bytedeco.javacpp.opencv_highgui.destroyAllWindows;
import static org.bytedeco.javacpp.opencv_highgui.imshow;
import static org.bytedeco.javacpp.opencv_highgui.waitKey;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import org.bytedeco.javacpp.indexer.UByteRawIndexer;
import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point2f;

import it.unipi.ing.mim.opencv.BoundingBox;
import it.unipi.ing.mim.opencv.Parameters;

import org.bytedeco.javacpp.indexer.FloatIndexer;
import org.bytedeco.javacpp.indexer.UByteBufferIndexer;


/*
*
* trova la trasformazione homography dell'oggetto rispetto al dataset.
*
* homography = findHomography(obj, scene, inliers, RANSAC, 1.0);
* obj è l'oggetto e scene è il dataset, inliers è l'output, il numero di inliers trovato (punti che matchano tra le due img e che non sono outlier)
* RANSAC è il metodo da utilizzare e 1.0 è la threshold.
*
*
*
* */

public class Ransac {

	private Mat homography;
	private Mat inliers;


	//TODO
	//compute the homography between two images
	public void computeHomography(DMatchVector goodMatches, KeyPointVector keypointsObject, KeyPointVector keypointsScene) {

		Mat obj = new Mat((int) goodMatches.size(), 1, CV_32FC2);
		Mat scene = new Mat((int) goodMatches.size(), 1, CV_32FC2);

		this.inliers = new Mat((int) Parameters.MIN_RANSAC_INLIERS, 1, CV_8UC1);



		FloatIndexer indexObj = obj.createIndexer();
		FloatIndexer indexScene = scene.createIndexer();

		for (int i = 0; i < goodMatches.size(); i++) {
				Point2f p1 = keypointsObject.get(goodMatches.get(i).queryIdx()).pt();
				Point2f p2 = keypointsScene.get(goodMatches.get(i).trainIdx()).pt();
				indexObj.put(i, p1.x(), p1.y());
				indexScene.put(i, p2.x(), p2.y());
		}


		homography = findHomography(obj, scene, inliers, RANSAC, Parameters.RANSAC_THRESHOLD);

	}

	public Mat getHomography() {
		return homography;
	}

	public Mat getInliers() {
		return inliers;
	}
	
	//TODO

	//il tipo Mat è una matrice, la cella è ad 1 se è un inliers e 0 se outlier quindi conta il numero di celle a 1

	public int countNumInliers() {

		UByteRawIndexer index = inliers.createIndexer();

		int num = 0;

		for (int x = 0; x < index.rows(); x++) {
			for (int y = 0; y < index.cols(); y++) {
				float values = index.get(x, y);
				if (values == 1)
					num++;
			}
		}
		return num;
	}

}
