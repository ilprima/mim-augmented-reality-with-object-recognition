package it.unipi.ing.mim.opencv;

import static org.bytedeco.javacpp.opencv_calib3d.projectPoints;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_highgui.waitKey;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.javacpp.indexer.FloatIndexer;


public class BoundingBox {

    public static void addBoundingBox(Mat imgScene, Mat imgTrainingObject, Mat homography, String tagTitle, Scalar color, boolean setOverlay) {
        addBoundingBox(imgScene, imgTrainingObject, homography, 0, tagTitle, color, setOverlay);
    }

    public static void addBoundingBox(Mat imgScene, Mat imgTrainingObject, Mat homography, int shift, String tagTitle, Scalar color, boolean setOverlay) {


        Mat objCorners = new Mat(4, 1, CV_32FC2);
        Mat sceneCorners = new Mat(4, 1, CV_32FC2);

        FloatIndexer objCornersIdx = objCorners.createIndexer();

        // System.out.println(i + " - " + p1.x() +"," + p1.y());
        objCornersIdx.put(0, 0, new float[]{0, 0});

        objCornersIdx.put(1, 0, new float[]{imgTrainingObject.cols(), 0});
        objCornersIdx.put(2, 0, new float[]{imgTrainingObject.cols(), imgTrainingObject.rows()});
        objCornersIdx.put(3, 0, new float[]{0, imgTrainingObject.rows()});

        // obj_corners:input
        perspectiveTransform(objCorners, sceneCorners, homography);

        int thick = 4;

        Scalar colorBB;
        if (color != null)
            colorBB = color;
        else
            colorBB = new Scalar(255, 255, 0, 0);

        int width = 0;
        int height = 0;


        FloatIndexer sceneCornersIdx = sceneCorners.createIndexer();

        int x0 = (int) sceneCornersIdx.get(0, 0, 0);
        int y0 = (int) sceneCornersIdx.get(0, 0, 1);
        x0 += shift;

        int x1 = (int) sceneCornersIdx.get(1, 0, 0);
        int y1 = (int) sceneCornersIdx.get(1, 0, 1);
        x1 += shift;
        line(imgScene, new Point(x0, y0), new Point(x1, y1), colorBB, thick, 3, 0);

        width = x0;
        height = y0;

        x0 = x1;
        y0 = y1;
        x1 = (int) sceneCornersIdx.get(2, 0, 0);
        y1 = (int) sceneCornersIdx.get(2, 0, 1);
        x1 += shift;
        line(imgScene, new Point(x0, y0), new Point(x1, y1), colorBB, thick, 3, 0);

        width = Math.abs(width - x1);
        height = Math.abs(height - y1);

        x0 = x1;
        y0 = y1;
        x1 = (int) sceneCornersIdx.get(3, 0, 0);
        y1 = (int) sceneCornersIdx.get(3, 0, 1);
        x1 += shift;
        line(imgScene, new Point(x0, y0), new Point(x1, y1), colorBB, thick, 3, 0);

        x0 = x1;
        y0 = y1;
        x1 = (int) sceneCornersIdx.get(0, 0, 0);
        y1 = (int) sceneCornersIdx.get(0, 0, 1);
        x1 += shift;
        line(imgScene, new Point(x0, y0), new Point(x1, y1), colorBB, thick, 3, 0);


        //put the tag in the bounding box
        Point point = new Point(x0, y1 - 50);
        double fontSize = 1.0;

        if (color == null)
            color = new Scalar(255, 0, 0, 0);


        if (tagTitle != null)
            putText(imgScene, tagTitle, point, thick, fontSize, colorBB);
        else
            putText(imgScene, "Nessun titolo", point,thick, fontSize, color);


        //crea il rettangolo
        x0 = (int) sceneCornersIdx.get(0, 0, 0);
        y0 = (int) sceneCornersIdx.get(0, 0, 1);

        Point pt1 = new Point(x0, y0);


        x1 = (int) sceneCornersIdx.get(2, 0, 0);
        y1 = (int) sceneCornersIdx.get(2, 0, 1) - 200;

        Point pt2 = new Point(x1, y1);
        //Point pt2 = new Point(180,120);


        int min = Math.min(height, width);

        min = Math.floorDiv(min, 2);


        if (setOverlay) {
            try {
                Mat img1 = imread("data/overlay/" + Constants.MAP_OVERLAY.get(tagTitle) + ".jpg");

                System.out.println("Widht: " + width + " Height: " + height);


                resize(img1, img1, new Size(min, min));
                overlayImage(imgScene, img1, imgScene, pt2);

                //put details
                double fontSizeDetails = 1.0;
                x1 = (int) sceneCornersIdx.get(2, 0, 0);
                y1 = (int) sceneCornersIdx.get(2, 0, 1);


                String text = Constants.MAP_DETAILS.get(tagTitle);
                int dy = 30;
                for (int i = 0; i < text.split("\n").length; i++) {
                    int y = y1 + i * dy;
                    Point pt3 = new Point(x1, y);
                    putText(imgScene, text.split("\n")[i], pt3, thick, fontSizeDetails, colorBB);
                }





            } catch (Exception e) {
                System.out.println("No overlay");
            }
        }









    }

    public static void imshow(String txt, Mat img) {
        CanvasFrame canvas = new CanvasFrame(txt);
        canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        OpenCVFrameConverter.ToMat toMat = new OpenCVFrameConverter.ToMat();
        canvas.showImage(toMat.convert(img));
        waitKey();
        canvas.dispose();

    }


    //doc http://jepsonsblog.blogspot.com/2012/10/overlay-transparent-image-in-opencv.html
    static void overlayImage(Mat background, Mat foreground, Mat output, Point location) {
        background.copyTo(output);


        // start at the row indicated by location, or at row 0 if location.y is negative.
        for (int y = Math.max(location.y(), 0); y < background.rows(); ++y) {
            int fY = y - location.y(); // because of the translation

            // we are done of we have processed all rows of the foreground image.
            if (fY >= foreground.rows())
                break;

            // start at the column indicated by location,

            // or at column 0 if location.x is negative.
            for (int x = Math.max(location.x(), 0); x < background.cols(); ++x) {

                int fX = x - location.x(); // because of the translation.

                // we are done with this row if the column is outside of the foreground image.
                if (fX >= foreground.cols())
                    break;

                // determine the opacity of the foreground pixel, using its fourth (alpha) channel.


                double opacity = 1;
                // and now combine the background and foreground pixel, using the opacity,


                // but only if opacity > 0.
                for (int c = 0; opacity > 0 && c < output.channels(); ++c) {
                    double foregroundPx = foreground.data().getDouble(fY * foreground.step() + fX * foreground.channels() + c);
                    double backgroundPx = background.data().getDouble(y * background.step() + x * background.channels() + c);

                    output.data().putDouble(y * output.step() + output.channels() * x + c, backgroundPx * (1.0 - opacity) + foregroundPx * opacity);
                }


            }
        }
    }
}















