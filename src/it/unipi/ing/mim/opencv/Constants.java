package it.unipi.ing.mim.opencv;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class Constants {

    public static final Map<String, String> MAP_OVERLAY = createMapOverlay();
    public static final Map<String, String> MAP_DETAILS = createMapDetails();


    private static Map<String, String> createMapOverlay() {
        Map<String, String> result = new HashMap<String, String>();

        //switch
        result.put("Odissey", "switch");
        result.put("BreathOfTheWild", "switch");
        result.put("XenoverseII", "switch");

        //DVD
        result.put("gangs", "dvd");
        result.put("hp1", "dvd");
        result.put("hp2", "dvd");
        result.put("lotr1", "dvd");
        result.put("contact", "dvd");

        //PS3
        result.put("hitman", "PS3");
        result.put("crysis3", "PS3");
        result.put("assassin'sCreed", "PS3");
        result.put("destiny", "PS3");


        //Libro
        result.put("LibroDataMining", "book");
        result.put("LibroDatabase", "book");
        result.put("JustEat", "book");

        //Bibite
        result.put("AcquaSanBenedetto", "drink");
        result.put("LatteViviVerde", "drink");
        result.put("AcquaSantAnna", "drink");



        return Collections.unmodifiableMap(result);
    }

    private static Map<String, String> createMapDetails() {
        Map<String, String> result = new HashMap<String, String>();

        //switch
        result.put("Odissey", "Uscita: 27 ottobre 2017\nPiattaforma: Nintendo Switch");
        result.put("BreathOfTheWild", "Uscita: 3 marzo 2017\nPiattaforma: Nintendo Switch");
        result.put("XenoverseII", "Uscita: 7 settembre 2017\nPiattaforma: Nintendo Switch");

        //DVD
        result.put("gangs", "Uscita: 20 dicembre 2002\nRegista: Martin Scorzese");
        result.put("hp1", "Uscita: 4 November 2001\nRegista: Chris Columbus");
        result.put("hp2", "Uscita: 3 November 2002\nRegista: Chris Columbus");
        result.put("lotr1", "Uscita: 10 December 2001\nRegista: Peter Jackson");
        result.put("contact", "Uscita: 11 luglio 1997\nRegista: Robert Zemeckis");

        //PS3
        result.put("hitman", "Uscita: 20 novembre 2012\nPiattaforma: PS3");
        result.put("crysis3", "Uscita: 19 febbraio 2013\nPiattaforma: PS3");
        result.put("assassin'sCreed","Uscita: 12 novembre 2012\nPiattaforma: PS3");
        result.put("destiny","Uscita: 9 settembre 2014\nPiattaforma: PS3");
        //Libro
        result.put("LibroDataMining", "Edizione: terza\nAutori: Han, Kamber, Pei");
        result.put("LibroDatabase", "book");
        result.put("JustEat", "Quaderno JustEat\nwww.justeat.it");

        //Bibite
        result.put("AcquaSanBenedetto", "Acqua minerale\nFonte: Scorze, Venezia");
        result.put("LatteViviVerde", "Latte Microfiltrato\nTipo: Parzialmente scremato");
        result.put("AcquaSantAnna", "Acqua minerale\nFonte: Vinadio, (CN)");



        return Collections.unmodifiableMap(result);
    }


}