package it.unipi.ing.mim.opencv.singleObject;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import it.unipi.ing.mim.opencv.BoundingBox;
import it.unipi.ing.mim.opencv.Parameters;
import it.unipi.ing.mim.opencv.localFeatures.*;

import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.*;

public class VideoObjectRecognition {

	private KeyPointsDetector detector = new KeyPointsDetector();
	private FeaturesExtraction extractor = new FeaturesExtraction();
	private FeaturesMatching matching = new FeaturesMatching();
	private FeaturesMatchingFiltered filter = new FeaturesMatchingFiltered();
	private Ransac ransac = new Ransac();

	private OpenCVFrameGrabber frameGrabber;

	public Mat imgTrainingObj;
	private KeyPointVector keypointsTrainingObj;
	private Mat descriptorTrainingObj;
	private Mat Objdescriptors;

	public CanvasFrame canvas;

	/*
	private static final String VIDEO_SRC = "data/img/videoTest.mp4";
	private static final int CAMERA_SRC = -1;

	private static final String OBJECT_IMG = "data/img/iphone.jpg";
*/

	private static final String VIDEO_SRC = "data/img/quaderno.mp4";
	private static final int CAMERA_SRC = -1;

	private static final String OBJECT_IMG = "data/img/quaderno_train.jpg";

	public static void main(String[] args) throws Exception {


		 VideoObjectRecognition objectRecognition = new VideoObjectRecognition(CAMERA_SRC, OBJECT_IMG);

		 objectRecognition.start();
	}
	


	public VideoObjectRecognition(int src, String obj) throws Exception {
		init(obj);
		System.out.println("Opening camera " + src);
		frameGrabber = OpenCVFrameGrabber.createDefault(0);
		frameGrabber.start();
	}


	private void init(String obj) {
		imgTrainingObj = imread(obj);
		keypointsTrainingObj = detector.detectKeypoints(imgTrainingObj);
		descriptorTrainingObj = extractor.extractDescriptor(imgTrainingObj, keypointsTrainingObj);
		canvas = new CanvasFrame("Object Recognition");
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
	}



    public void start() throws org.bytedeco.javacv.FrameGrabber.Exception {
		OpenCVFrameConverter.ToMat toMat = new OpenCVFrameConverter.ToMat();

		int frameCounter = 0;
		Frame frame;
		while ((frame = frameGrabber.grabFrame()) != null) {
			if (Parameters.FRAME_FREQ == 0 || frameCounter++ % Parameters.FRAME_FREQ == 0) {
				Mat matFrame = toMat.convert(frame);
				Mat homography = homography(matFrame);

				if (homography != null) {
					BoundingBox.addBoundingBox(matFrame, imgTrainingObj, homography, null, null,Parameters.setOverlay);
				}
				canvas.showImage(toMat.convert(matFrame));
			}
		}
		canvas.dispose();
	}

	public Mat homography(Mat frame) {
		Mat homography = null;

		// detect keypoints from frame image

		KeyPointVector keypoints = detector.detectKeypoints(frame);

		// extract features from keypoints

		Mat descQuery = extractor.extractDescriptor(frame, keypoints);

		// match and filter features with the features (descriptorObj) of the query

		DMatchVector matches = matching.match(descriptorTrainingObj, descQuery);

		DMatchVector goodMatching = filter.matchWithFiltering(matches, Parameters.MAX_DISTANCE_THRESHOLD);


		// use computeHomography the check the Homography


		if (goodMatching.size()>Parameters.MIN_GOOD_MATCHES) {
			ransac = new Ransac();
			ransac.computeHomography(goodMatching, keypointsTrainingObj, keypoints);
			System.out.println("goodMatching > min: " + goodMatching.size());

		// and return it if there are enough

			homography = ransac.getHomography();
			System.out.println("conto num Inliers: " + ransac.countNumInliers() );


			if (ransac.countNumInliers() >= Parameters.MIN_RANSAC_INLIERS)
				return homography;
		}
			return null;
	}



}