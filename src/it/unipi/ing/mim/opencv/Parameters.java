package it.unipi.ing.mim.opencv;

import java.io.File;

public class Parameters {

    public static final int FRAME_FREQ = 0;

    public static final int MIN_RANSAC_INLIERS = 12;
    public static final double RANSAC_THRESHOLD = 1.0;

    public static final int MIN_GOOD_MATCHES = 15;

    public static final int MAX_DISTANCE_THRESHOLD = 34;

    public static final File SRC_FOLDER = new File("data/dataset");


    //VARIABILI GLOBALI PER PROGETTO
    public static final int splitFactor = 16;
    public static final int maxDimFastIndex = 3;
    public static final boolean setOverlay = true;


    public static final String appTitle = "AR Recognition";
    public static final String videoFromFilePath = "C:\\Users\\Simone\\Desktop\\prova.mp4";
}